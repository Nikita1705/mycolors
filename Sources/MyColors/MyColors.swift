
import UIKit

public enum Colors: String {
    case Apricot = "FBCEB1"
    case ApricotCrayola = "FDD9B5"
    case AgateGray = "B5B8B1"
    case Aquamarine = "7FFFD4"
    case AquamarineCrayola = "78DBE2"
    case AlizarinRed = "E32636"
    case Scarlet = "FF2400"
    case AmaranthPurple = "AB274F"
    case AmaranthPink = "F19CBB"
    case Amaranth = "E52B50"
    case AmaranthDeepPurple = "9F2B68"
    case AmaranthMagenta = "ED3CCA"
    case AmaranthLightCherry = "CD2682"
    case AmericanPink = "FF033E"
    case Amethyst = "9966CC"
    case AntiqueBrass = "CD9575"
    case AnthraciteGray = "293133"
    case Anthracite = "464451"
    case Harlequin = "44944A"
    case SlateGrey = "2F4F4F"
    case SlateBlue = "6A5ACD"
    case GrandmasApples = "A8E4A0"
    case BasaltGrey = "4E5754"
    case BurningSand = "CD9A7B"
    case Graphite = "1E1112"
    case CoralTree = "B76E79"
    case Redwood = "592321"
    case BrownishGrey = "8B6C62"
    case DarkHotPink = "D71868"
    case BullShot = "7D512D"
    case Iridium = "414833"
    case Sinopia = "C04000"
    case AubergineCrayola = "614051"
    case Eggplant = "990066"
    case EggplantCrayola = "6E5160"
    case Bananomania = "FAE7B5"
    case PeriwinklePervansh = "CCCCFF"
    case PeriwinkleCrayola = "C5D0E6"
    case ThighsFrightenedNymph = "FAEEDD"
    case BeigeBrown = "79553D"
    case BeigeRed = "C1876B"
    case BeigeGrey = "6D6552"
    case Beige = "F5F5DC"
    case WhiteAluminum = "A5A5A5"
    case WhiteGreen = "BDECB6"
    case SnowWhite = "FFFAFA"
    case White = "FFFFFF"
    case WhiteAntique = "FAEBD7"
    case WhiteNavajo = "FFDEAD"
    case DarckBlue = "003153"
    case TurquoiseBlueCrayola = "77DDE7"
    case TurquoiseGreen = "1E5945"
    case TurquoiseBlue = "3F888F"
    case Turquoise = "30D5C8"
    case Biscuit = "FFE4C4"
    case BismarckFurioso = "A5260A"
    case Bistre = "3D2B1F"
    case FaintlyCornflowerBlue = "ABCDEF"
    case FaintlyYellow = "FFDB8B"
    case FaintlyGreenGrey = "8D917A"
    case FaintlyGreen = "89AC76"
    case FaintlyGolden = "EEE8AA"
    case FaintlyCarmine = "B03F35"
    case FaintlyChestnut = "DDADAF"
    case FaintlyBrown = "755C48"
    case FaintlySandy = "DABDAB"
    case FaintlyViolet = "F984E5"
    case FaintlyPinkish = "FFCBDB"
    case FaintlyPink = "FADADD"
    case FaintlyBlue = "AFEEEE"
    case FaintlyPurple = "957B8D"
    case PaleSpringBud = "ECEBBD"
    case PaleYellowGreen = "F0D698"
    case PaleYellowPink = "FFC8A8"
    case PaleGreenishYellow = "FFDF84"
    case PaleGreen = "98FB98"
    case PaleRedPurple = "AC7580"
    case PaleoOrangeYellow = "FFCA86"
    case PalePurplishPink = "FDBDBA"
    case PalePurpleBlue = "8A7F8E"
    case PaleGreyBrown = "BC987E"
    case PaleBlue = "919192"
    case PalePurpleRed = "D87093"
    case Wattle = "CED23A"
    case Mantis = "8CCB5E"
    case BrightSun = "FFCF40"
    case Lamplight = "FFDC33"
    case JellyBean = "2A8D9C"
    case ChateauGreen = "47A76A"
    case YellowOrange = "FFB841"
    case SairaRed = "FF97BB"
    case Kimberly = "62639B"
    case LavenderPink = "DD80CC"
    case GreenHaze = "009B76"
    case CyanBlue = "4285B4"
    case Deluge = "755D9A"
    case Czarina = "755A57"
    case Pharlap = "9F8170"
    case BulgarianRose = "480607"
    case SwampGreen = "ACB78E"
    case BrightRed = "B00000"
    case WineBerry = "641C34"
    case Stiletto = "9B2D30"
    case Alto = "D5D5D5"
    case GrayAsparagus = "4C514A"
    case Chambray = "3E5F8A"
    case MySin = "FFB02E"
    case BrandyPunch = "CD7F32"
    case Burgundy = "900020"
    case Cedar = "45161C"
    case Rangitoto = "343B29"
    case RedDamask = "D5713F"
    case CornflowerBlue = "6495ED"
    case Cornflower = "9ACEEB"
    case Chenin = "DAD871"
    case Lima = "34C924"
    case Cranberry = "DE4C8A"
    case SpringGreen = "00FF7F"
    case FallGreen = "ECEABE"
    case SpringBud = "A7FC00"
    case MediumRedViolet = "BD33A4"
    case Finn = "702963"
    case BurntCrimson = "5E2129"
    case Claret = "911E42"
    case RawUmber = "64400F"
    case Calypso = "256D7B"
    case BondiBlue = "0095B6"
    case SunglowGecko = "FFCF48"
    case HeatheredGray = "B8B799"
    case Gainsboro = "DCDCDC"
    case Heliotrope = "DF73FF"
    case YellowSea = "F3A505"
    case PickledBean = "734222"
    case LightWisteria = "C9A0DC"
    case SpringWistera = "CDA4DE"
    case UnloadedTexturePurple = "C154C1"
    case Jambalaya = "593315"
    case TartOrange = "F64A46"
    case Camarone = "00541F"
    case PirateGold = "B57900"
    case Dell = "425E17"
    case Pestilence = "9F8200"
    case KaitokeGreen = "004524"
    case Pomegranate = "EF3038"
    case MexicanRed = "A9203E"
    case CoralRed = "FF4040"
    case VanCleef = "4D220E"
    case Temptress = "490005"
    case EvilCencipede = "A91D11"
    case Loulou = "641349"
    case VenetianRed = "7B001C"
    case DeepFir = "142300"
    case Bamboo = "D76E00"
    case RockSpray = "C34D0A"
    case Siren = "6F0035"
    case FrenchRose = "EB5284"
    case Haiti = "1A153F"
    case CosmicExplorer = "531A50"
    case SecretStory = "FF1493"
    case DeepTeal = "00382B"
    case NightDive = "002F55"
    case Violet = "240935"
    case Minsk = "423189"
    case Lynch = "606E8C"
    case DodgerBlue = "42AAFF"
    case Cerulean = "00BFFF"
    case Wistful = "A2A2D0"
    case Spray = "80DAEB"
    case BlueZodiac = "0E294B"
    case Shamrock = "30BA8F"
    case Seagull = "87CEEB"
    case Mustard = "FFDB58"
    case Salmon = "FD7C6E"
    case ChinesGoldfish = "F34723"
    case OuterSpace = "2F353B"
    case CodGray = "1C1C1C"
    case Abbey = "474A51"
    case PowderAsh = "C7D0CC"
    case RedViolet = ""
    case Pear = "D1E231"
    case GoldenDream = "EFD334"
    case Gamboge = "E49B0F"
    case Sulu = "B2EC5D"
    case FunGreen = "00693E"
    case Hibiscus = "CA3767"
    case Denim = "1560BD"
    case WildStrawberry = "FF43A4"
    case UltraRed = "FC6C85"
    case RockBlue = "A2ADD0"
    case WildSand = "F5F5F5"
    case WebOrange = "F4A900"
    case Melon = "FDBCB4"
    case CapeCod = "434B4D"
    case Broom = "EDFF21"
    case CitrusSpice = "E1CC4F"
    case Atlantis = "9ACD32"
    case YellowGreen = "C5E384"
    case SinCity = "CDA434"
    case Mondo = "47402E"
    case Christine = "ED760E"
    case MandarinSorbet = "FFAE42"
    case PeachYellow = "FADFAD"
    case Peach = "FFE4B2"
    case ClayCreek = "8F8B66"
    case Cookie = "FFE2B7"
    case SorrellBrown = "CAA885"
    case Yellow = "FFFF00"
    case Olive = "9D9101"
    case SweetCorn = "FCE883"
    case BuddhaGold = "D6AE01"
    case AthsSpecial = "EAE6CA"
    case BurntOrange = "CC5500"
    case Charm = "CB6586"
    case ClearChill = "1E90FF"
    case CamouflageGreen = "78866B"
    case RazzleDazzleRose = "FF47CA"
    case Chartreuse = "7CFC00"
    case PineGreen = "01796F"
    case Genoa = "158078"
    case Gimblet = "BEBD7F"
    case GreenYellow = "ADFF2F"
    case LamonFlesh = "F0E891"
    case Pesto = "826C34"
    case Lime = "BFFF00"
    case CabbagePont = "4D5645"
    case TePapaGreen = "1F3438"
    case EphrenBlue = "1164B4"
    case AlbescentWhite = "F5E6CB"
    case Pablo = "7A7666"
    case KokushokuBlack = "181513"
    case Nandor = "4E5452"
    case LakeGreen = "2E8B57"
    case Keppel = "3BB08F"
    case JungleGreen = "29AB87"
    case JapaneseLaurel = "008000"
    case MountainMeadow = "1CAC78"
    case MossGreen = "ADDFAD"
    case TreasuredWilderness = "006633"
    case LunarGreen = "2F4538"
    case SherpaBlue = "004953"
    case FernGreen = "4F7942"
    case GreenCyan = "009A63"
    case TeaGreen = "D0F0C0"
    case Goldenrod = "FCD975"
    case GoldenGrass = "DAA520"
    case Nutmeg = "712F26"
    case Gold = "FFD700"
    case GoldSand = "E7C697"
    case Tamarind = "321414"
    case Ironstone = "79443B"
    case Cadaverous = "009B77"
    case GreenPea = "287233"
    case Emerald = "50C878"
    case PigmentIndigo = "4B0082"
    case Indigo = "5D76CB"
    case IndiaGreen = "138808"
    case ChestnutRose = "CD5C5C"
    case Christi = "4CBB17"
    case Conifer = "BDDA57"
    case BreakerBay = "5F9EA0"
    case CadetBlue = "B0B7C6"
    case Paarl = "A25F2A"
    case Bitter = "8B8C7A"
    case PaleCanary = "FFFF99"
    case Blumine = "1B5583"
    case Cardinal = "C41E3A"
    case Java = "1CD3A2"
    case Carmine = "960018"
    case RedMenace = "A2231D"
    case Cinnabar = "EB4C42"
    case TorchRed = "FF0033"
    case Buccaneer = "633A34"
    case FuzzyWuzzyBrown = "BC5D58"
    case Zorba = "99958C"
    case Flint = "6C6960"
    case Vermilion = "FF4D00"
    case FlushMahogany = "CB4154"
    case SanguineBrown = "884535"
    case Carpaccio = "E34234"
    case FernFrond = "507D2A"
    case Valencia = "D53032"
    case Mirage = "1E213D"
    case Cobalt = "0047AB"
    case Buff = "F0DC82"
    case OrangePeel = "FFA000"
    case RoofTerracotta = "B32821"
    case Coral = "FF7F50"
    case Tosca = "893F45"
    case Desert = "B15124"
    case SweetAlmond = "SWEET ALMOND"
    case LiverBrown = "503D33"
    case Optophobia = "140F0B"
    case CraftPaper = "8A6642"
    case HarissaRed = "A52A2A"
    case WobbegongBrown = "C19A6B"
    case Taupe = "39352A"
    case FaluRed = "781F19"
    case Maroon = "800000"
    case CherryLolly = "C8385A"
    case OrkaBlack = "25221B"
    case Brownie = "964B00"
    case BrownWood = "B4674D"
    case VermilionGreen = "464531"
    case SaddleBrown = "8B4513"
    case HazelnutChocolate = "7B3F00"
    case RoyalFuchsia = "CA2C92"
    case RoyalLavender = "7851A9"
    case RoyalBlue = "4169E1"
    case CosmicLate = "FFF8E7"
    case DarkSpace = "414A4C"
    case MoroccoBrown = "442D25"
    case ForgottenSunset = "FDD5B1"
    case Tenne = "CD5700"
    case Russet = "80461B"
    case Gazpacho = "C93C20"
    case RedOrangeJuice = "FF5349"
    case RoyalLines = "6D3F5B"
    case SehnsuchtRed = "922B3E"
    case BoatOrchid = "C0448F"
    case AfricanMahogany = "CD4A4C"
    case Red = "FF0000"
    case RedCrayon = "EE204D"
    case SandyBrown = "F4A460"
    case YellowUrnOrchid = "FFFDD0"
    case HistoricCrean = "FDF4E3"
    case Khaki = "C3B091"
    case GoblinGreen = "76FF7A"
    case BrinkPink = "FB607F"
    case VividOrange = "E4A010"
    case Corn = "FBEC5D"
    case CyberLavender = "E6E6FA"
    case LavenderCandy = "FCB4D5"
    case LavenderRose = "FBA0E3"
    case PressedLaserLemon = "FEFE22"
    case CeladonBlue = "007BA7"
    case Plunge = "025669"
    case Azure = "007FFF"
    case BrightCerulean = "1DACD6"
    case Green = "00FF00"
    case PoisonousPesticide = "32CD32"
    case BrightManatee = "979AAA"
    case Brass = "B5A642"
    case EasternWolf = "DBD7D2"
    case SummerForestGreen = "228B22"
    case Liver = "534B4F"
    case Sickener = "DB7093"
    case HepGreen = "C7B446"
    case YellowCattleya = "FFF44F"
    case LemonChiffon = "FFFACD"
    case GenoaLemon = "FDE910"
    case Pine = "2D572C"
    case MorningForest = "6DAE81"
    case FireIsland = "D95030"
    case MandarinRed = "E55137"
    case SalmonOrange = "FF8C69"
    case SalmonTartare = "FF9BAA"
    case Linen = "FAF0E6"
    case Encarnado = "F80000"
    case HoneyWax = "FFA420"
    case AmazonMoss = "7B917B"
    case MagicMint = "AAF0D1"
    case SugarCristal = "F8F4FF"
    case Magenta = "FF00FF"
    case FugitiveFlamingo = "F664AF"
    case CottonMuslin = "EDD19C"
    case MayGreen = "4C9141"
    case ReddishBanana = "FFBD88"
    case VividCyan = "0BDA51"
    case MovieStar = "C51D34"
    case Irresistible = "B3446C"
    case WildRiderRed = "DC143C"
    case ScarletFrame = "993366"
    case MangoTango = "FF8243"
    case Tigerlily = "E1523D"
    case MandarineJelly = "FF8800"
    case Property = "4C5866"
    case ColonialBlack = "AD655F"
    case Acajou = "4C2F27"
    case Fire = "8E402A"
    case CopperRose = "996666"
    case Copper = "B87333"
    case BuffedCopper = "DD9475"
    case Honeydew = "F0FFF0"
    case ChestnutGold = "A98307"
    case PalePeach = "FEE5AC"
    case AerospaceOrange = "FF4F00"
    case DarkImperialBlue = "00416A"
    case ToastedOatmeal = "EFDECD"
    case Myrtle = "21421E"
    case LoveVessel = "EF0097"
    case FashionFuchsia = "F400A1"
    case HopiMoccasin = "FFE4B5"
    case TropicalGreen = "17806D"
    case DurotarFire = "F36223"
    case SeaGreen = "54FF9F"
    case BrightSeaGreen = "9FE2BF"
    case PeacockPlume = "1C6B72"
    case IslamicGreen = "009900"
    case CannonGray = "646B63"
    case MintGreen = "98FF98"
    case AzuriteWaterGreen = "497E76"
    case Greenbrair = "20603D"
    case MintCream = "F5FFFA"
    case Mint = "3EB489"
    case OrangeSatisfaction = "DC9D00"
    case FoggyBog = "7F8F18"
    case BurntBagel = "95500C"
    case PeachyPinky = "FF7A5C"
    case OvergrownMausoleum = "478430"
    case ClarifiedButter = "E59E1F"
    case LemonCurry = "CCA817"
    case CaribbeanSplash = "00677E"
    case CadmiumGreen = "006B3C"
    case BeerGlazedBacon = "753313"
    case GrubbyRed = "7F180D"
    case PeanutButterChicken = "FFB961"
    case PowderRed = "9A366B"
    case Relief = "BF2233"
    case IndustrialGreen = "0A4500"
    case LadduOrange = "FF8E0D"
    case ChaatMasala = "EC7C26"
    case MintedBlueberryLemonade = "B32851"
    case Carnation = "F6768E"
    case Liberty = "474389"
    case ForbiddenFruit = "FD7B7C"
    case ShadyGlade = "006D5B"
    case DarkDenim = "00538A"
    case PrismViolet = "53377A"
    case HairyBrown = "734A12"
    case Vapor = "F0FFFF"
    case Planetarium = "2271B3"
    case ChromisDamselBlue = "7FC7FF"
    case Scallion = "6B8E23"
    case LaserLemon = "FFFF66"
    case RadiationCarrot = "FFA343"
    case Jade = "00A86B"
    case PoetryReading = "9DB1CC"
    case Wahoo = "252850"
    case MediumSpringsBug = "C9DC87"
    case RoastedSiena = "EA7E5D"
    case UpsedTomato = "AF2B1E"
    case MythicalOrange = "FF7F49"
    case Hawkbit = "FDDB6D"
    case ManganeseBlack = "1D334A"
    case FullCityRoast = "642424"
    case KuroBrown = "59351F"
    case AncientMaze = "999950"
    case CountryHouseGreen = "424632"
    case OliveOil = "BAB86C"
    case ChocolateRain = "6F4F28"
    case Darkness = "121910"
    case HeartGold = "808000"
    case LimoniteBrown = "4D4234"
    case AlpineGreen = "015D52"
    case OperaMauve = "B784A7"
    case SunsetOrange = "FD5E53"
    case LighthouseGlow = "F8D568"
    case BeefHotpot = "A65E2E"
    case Pelati = "FF2B2B"
    case PeachOrange = "FFCC99"
    case AtomicTangerine = "FF9966"
    case Orange = "FFA500"
    case SyntheticPumpkin = "FF7538"
    case WeatheredBamboo = "5B3A29"
    case AuraOrange = "B32428"
    case PinkOrchid = "DA70D6"
    case LightOrchid = "E6A8D7"
    case SelectiveYellow = "FFBA00"
    case DutchBlue = "49678D"
    case VerdeGarrafa = "355E3B"
    case Ochre = "CC7722"
    case QueenPalm = "AEA04B"
    case AfghanCarpet = "955F20"
    case IceHotPink = "E6BBC1"
    case CleanAir = "D8DEBA"
    case QueensViolet = "CBBAC5"
    case Rediscover = "C1CACA"
    case Mauvelous = "D8B1BF"
    case TidesOfDarkness = "002800"
    case PionPurple = "470736"
    case SavingLight = "4F0014"
    case DarkSanctuary = "470027"
    case Synallactida = "320B35"
    case SekichikuPink = "E3A9BE"
    case Coincidence = "C6DF90"
    case AspiringBlue = "A3C6C0"
    case KettleDrum = "98C793"
    case VioletBouquet = "BAACC7"
    case NihilakhOxide = "A0D6B4"
    case ClearLakeTrail = "A6BDD7"
    case LoveCloud = "EEBEF1"
    case Inside = "230D21"
    case BloedWorst = "560319"
    case NoriGreen = "132712"
    case StellarExplorer = "022027"
    case BlackEmerald = "16251C"
    case MeskiBlack = "270A1F"
    case BlackRooster = "320A18"
    case BrownTumbleweed = "362C12"
    case Aubergine = "28071A"
    case ClockChimesThirteen = "001D18"
    case AvocadoStone = "4C3C18"
    case BlanchedAlmond = "FFEBCD"
    case MintCoffee = "C7FCEC"
    case YoungFern = "71BC78"
    case Greenhouse = "3D642D"
    case KingfisherTorquoise = "7FB5B5"
    case EgyptianGold = "EFA94A"
    case PastelGreen = "77DD77"
    case DarkSparks = "FF7514"
    case PastelPink = "FFD1DC"
    case JugendstilTorquoise = "5D9B9B"
    case BerryBright = "A18594"
    case PokerGreen = "316650"
    case Tumbleweed = "DEAA88"
    case AgingBarrel = "6A5D4D"
    case DeepSeaDolphin = "6C6874"
    case LimeGreen = "1C542D"
    case RusticCabin = "705335"
    case RaucousOrange = "C35831"
    case RagingBull = "B44C43"
    case HeavyHeart = "721422"
    case SmashedGrape = "8673A1"
    case LoonTorquoise = "2A6478"
    case BurnishedRusset = "763C28"
    case Crust = "898176"
    case AmbassadorBlue = "102C54"
    case OreBluishBlack = "193737"
    case CodexGray = "9C9C9C"
    case TrolleyGrey = "828282"
    case PersianGreen = "00A693"
    case PersianIndigo = "32127A"
    case PersianRed = "CC3333"
    case PersianRose = "FE28A2"
    case electricIndigo = "6600FF"
    case MouseNose = "FFE5B4"
    case PeachJuice = "FFCFAB"
    case Peru = "CD853F"
    case DesertField = "EFCDB8"
    case SpringRoll = "C6A664"
    case GlossyGold = "FCDD76"
    case BistreBrown = "967117"
    case GreenPigment = "00A550"
    case HeavyMetall = "31372B"
    case RichRed = "F8173E"
    case JubileeGrey = "7F7679"
    case BiogenicSand = "FFEFD5"
    case ShadowWoods = "8A795D"
    case PrussianBlue = "003366"
    case BrightMidnightBlue = "1A4876"
    case LuckyPoint = "191970"
    case CarrotCurl = "FF9218"
    case CursedBlack = "131313"
    case GhostWhite = "F8F8FF"
    case HotMagenta = "FF00CC"
    case MuffinMagic = "FADBC8"
    case DarkRed = "75151E"
    case UmbraSend = "88706B"
    case OceanAbyss = "20155E"
    case Blackberry = "4A192C"
    case BatsCloak = "1B1116"
    case PurpleMountainsMajesty = "9D81BA"
    case PurpleRain = "7442C8"
    case Purple = "800080"
    case GruyereCheese = "F5DEB3"
    case PlasticCarrot = "F75E25"
    case TicklePink = "FF7E93"
    case MediumGrey = "7D7F7D"
    case PowderBlue  = "B0E0E6"
    case Puce = "CC8899"
    case LoveRed = "FF496C"
    case Yellowl = "F3DA0B"
    case Kale = "587246"
    case MartianIroncrust = "B7410E"
    case Piggy = "EF98AA"
    case HowdyPartner = "C8A696"
    case CarnationPink = "FFAACC"
    case YoungRedwood = "AB4E52"
    case FuchsiaPink = "FF77FF"
    case RosyBrown = "BC8F8F"
    case LavenderBlush = "FFF0F5"
    case RoseTaupe = "905D5D"
    case MamiePink = "EE82EE"
    case RoseEbony = "674846"
    case LightPink = "FFC0CB"
    case SoftPink = "FC89AC"
    case LipstickIllusion = "D36E70"
    case PinkOrthoclase = "AA98A9"
    case Rosewood = "65000B"
    case MountbattenPink = "997A8D"
    case FreshPiglet = "FDDDE6"
    case ElectricFlamingo = "FC74FD"
    case UnripeStrawberry = "F78FA7"
    case RubyRed = "9B111E"
    case BlushDiamure = "DE5D83"
    case YellowMandarin = "D77D31"
    case BrightMint = "99FF99"
    case Chokecherry = "92000A"
    case BlackHole = "1D1E33"
    case DarkSapphire = "082567"
    case CeriseRed = "DE3163"
    case PaleLavender = "DCD0FF"
    case HotterButter = "E28B00"
    case DamsonPlum = "DDA0DD"
    case CopacabanaSand = "E6D690"
    case FreshTorquoise = "40E0D0"
    case PrunusAvium = "DD4492"
    case SeasDrift = "87CEFA"
    case WinterDuvet = "FFFFE0"
    case Utepils = "FAFAD2"
    case UlvaLactucaGreen = "90EE90"
    case QuackQuack = "FFEC8B"
    case EgyptianJavelin = "FFBCAD"
    case DecreasingBrown = "987654"
    case CarrotOrange = "ED9121"
    case YukonGold = "846A20"
    case SpringRite = "FDEAA8"
    case PurpleCheeks = "BA7FA2"
    case LuminescentPink = "F984EF"
    case MattPink = "FFB6C1"
    case WornSilver = "C9C0BB"
    case GravelFint = "BBBBBB"
    case BlossomBlue = "A6CAF0"
    case TomatoQueen = "D84B20"
    case BaronessMauve = "876C99"
    case BlueGlass = "20B2AA"
    case Somnambulist = "778899"
    case EveningStar = "FFD35F"
    case MattBlue = "2B6CC4"
    case LimeTree = "DCD36A"
    case Canvas = "BB8B54"
    case CoralDusk = "FFB28B"
    case BritishKhaki = "BAAF96"
    case RodanGold = "FFDE5A"
    case DustyTorquoise = "649A9E"
    case Emberglow = "E66761"
    case SweetGeorgiaBrown = "8B6D5C"
    case Amazonian = "A86540"
    case SohoRed = "AA6651"
    case PinkyPickle = "BB6C8A"
    case RedRadish = "E63244"
    case FreshCinnamone = "945D0B"
    case VerderMarron = "887359"
    case SpiceOrange = "FFA161"
    case MakeUpBase = "FFA8AF"
    case TeatimeMauve = "C8A99E"
    case SecondPour = "837DA2"
    case PowderyPink = "EA899A"
    case Syrup = "B48764"
    case SableBrown = "946B54"
    case TimelessCopper = "966A57"
    case BrickDust = "B17267"
    case DessertWillow = "8B734B"
    case HorizonGlow = "B27070"
    case Riverbed = "84C3BE"
    case CapeHope = "D7D7D7"
    case GrandVillage = "6C92AF"
    case ScotchLassie = "669E85"
    case BlindDate = "BEADA1"
    case LightBlue = "ADD8E6"
    case LightSteelBlue = "B0C4DE"
    case AncestralWater = "D0D0D0"
    case Cornflake = "F0E68C"
    case LightCyan = "E0FFFF"
    case Celadon = "ACE1AF"
    case Sepia = "704214"
    case VanillaBrown = "382C1E"
    case HotCacao = "A5694F"
    case RegentGrey = "78858B"
    case GreyAsparagus = "465945"
    case HolyCrow = "332F2C"
    case Atmospheric  = "8A9597"
    case Silver = "C0C0C0"
    case SilverCross = "CDC5C2"
    case GreenLentils = "9E9764"
    case Affair = "735184"
    case OreganoDash = "90845B"
    case ChocolateCaliente = "785840"
    case DustyCoral = "D39B85"
    case CaribouHerb = "CEA262"
    case ModerateOrange = "C4A55F"
    case NottinghamForest = "575E4E"
    case MilkChocolate = "5A3D30"
    case FairyBrown = "5E3830"
    case SunBakedBrick = "B85D43"
    case BaroqueRed = "7D4D5D"
    case RojoOxid = "8C4743"
    case DeepBronze = "52442C"
    case RugbyTan = "C2A894"
    case DullMagenta = "8C4852"
    case DucalPink = "CC9293"
    case BlueberryPie = "413D51"
    case RubyGray = "72525C"
    case Artifact = "CF9B8F"
    case WinterSolstice = "4A545C"
    case Mysterious = "46394B"
    case DarkDesaturatedYellow = "#48442D"
    case RawStone = "9DA1AA"
    case ClearGrey = "808080"
    case NatureGate = "686C5E"
    case VintageGlass = "CADABA"
    case Eclipse = "403A3A"
    case ElephantGrey = "95918C"
    case FlindersGreen = "6C7059"
    case SilverSconce = "A0A0A4"
    case BlackOlive = "3E3B32"
    case ClosedShutter = "26252D"
    case Saguaro = "6A5F31"
    case DuneGrass = "CAC4B0"
    case GreyChain = "708090"
    case FatGold = "E5BE01"
    case GreenGrass = "317F43"
    case RedMane = "6C3B2A"
    case Ketchup = "A52019"
    case VitaminC = "FF9900"
    case AcaciaHaze = "969992"
    case SchiavoBlue = "1E2460"
    case VerveViolet = "924E7D"
    case DireWolf = "282828"
    case TexasBrown = "A0522D"
    case Bloodletter = "E97451"
    case TwilightBlue = "79A0C1"
    case PonderosaPine = "1F3A3D"
    case SereneBlue = "0D98BA"
    case BrightBlueViolet = "8A2BE2"
    case OtherBlue = "6699CC"
    case SpiceBlum = "6C4675"
    case UltraViolet = "7366BD"
    case Arrowroot = "F9DFCF"
    case BearSuit = "7D746D"
    case PianoBlack = "151719"
    case ClearBlue = "0000FF"
    case DropletBlue = "AFDAFC"
    case BlueAzure = "007DFF"
    case CelestialPlum = "3A75C4"
    case CloudedBlue = "1F75FE"
    case BlackBay = "474B4E"
    case BlueMartini = "1FCECB"
    case CalmBlueSea = "18A7B5"
    case EgyptianBlue = "122FAA"
    case CeruleanBlue = "2A52BE"
    case Smalt = "003399"
    case SteelBlue = "4682B4"
    case AliceBlue = "F0F8FF"
    case StormySunrise = "C8A2C8"
    case PearlyPurple = "B565A7"
    case OutrageousOrange = "FF6E4A"
    case MediumScarlet = "FC2847"
    case CottonCandy = "FFBCD9"
    case MidnightSky = "434750"
    case PurpleDreamer = "660066"
    case PlumPie = "8E4585"
    case CreamyCreamy = "F2DDC6"
    case Creamy = "F2E8C9"
    case Ivory = "FFFFF0"
    case Uranus = "ACE5EE"
    case ShinyGold = "F39F18"
    case SalmonSlice = "EFAF8C"
    case FreshMint = "2C5545"
    case DeepChestnut = "B94E48"
    case Frankenstein = "7BA05B"
    case PeeledAsparagus = "87A96B"
    case MediumCarmine = "AF4035"
    case MediumPersianBlue = "0067A5"
    case PurpleMatt = "9370D8"
    case PineBark = "817066"
    case BlackHalo = "231A24"
    case OldRose = "C08081"
    case OldGold = "CFB53B"
    case OldLace = "FDF5E6"
    case Flax = "EEDC82"
    case BrightSienna = "D68A59"
    case MonksRobe = "714B23"
    case Tin = "909090"
    case PurpleFrame = "CF3476"
    case BruisedBear = "5D3954"
    case PurpleVanity = "9932CC"
    case FlashMahogany = "CB2821"
    case EmeraldPool = "116062"
    case KingTriton = "3B83BD"
    case ChineseGold = "D8A903"
    case BronzeWeapon = "B07D2B"
    case NightlyWoods = "013220"
    case UnfiredClay = "986960"
    case BugmanGlow = "CD5B45"
    case SatoimoBrown = "654321"
    case ScabRed = "8B0000"
    case LostRiver = "08457E"
    case KindledFlame = "E9967A"
    case CloudyCarrot = "FFA812"
    case FigLeaf = "556832"
    case SunCrete = "FF8C00"
    case PeachPuff = "FFDAB9"
    case WinterBloom = "472A3F"
    case HeatherBerry = "E75480"
    case MauveTaupe = "915F6D"
    case AncientLavastone = "483C32"
    case Sealskin = "49423D"
    case MidnightDreams = "002137"
    case NavyBlue = "000080"
    case BrightNavyBlue = "1974D2"
    case VioletInk = "9400D3"
    case TikiMonster = "8FBC8F"
    case NauseousBlue = "483D8B"
    case CloudyCamouflage = "177245"
    case Hype = "57A639"
    case SyndicateCamouflage = "918151"
    case CarryDash = "CC6C5C"
    case ChestnutGreen = "304B26"
    case Sambuca = "3F2512"
    case AimiruBrown = "313830"
    case LuxorGold = "9B8127"
    case VolcanicStoneGreen = "45433B"
    case NaturalIndigo = "003841"
    case PalmGreen = "203A27"
    case BreachedSpruce = "BADBAD"
    case HotToddy = "B8860B"
    case DeepViolet = "310062"
    case Paco = "35170C"
    case CoffeeBean = "321011"
    case ChocolateLust = "9B2F1F"
    case PickledBeet = "4F273A"
    case RoyalBrown = "523C36"
    case PersianPlum = "681C23"
    case DarkMagenta = "8B008B"
    case ThaiMango = "EA7500"
    case DarkestForest = "232C16"
    case BlackSlug = "302112"
    case DesertSun = "C37629"
    case Malachite = "03C03C"
    case LargestBlackSlug = "5B1E31"
    case TripleBerry = "C76574"
    case WoodBlackRed = "564042"
    case Sambucus = "1A162A"
    case IndigoPurple = "660099"
    case StormyPassion = "C76864"
    case WoodThrush = "A47C45"
    case KenpozomeBlack = "32221A"
    case BlackKite = "371F1C"
    case BrownCoffee = "482A2A"
    case Zeus = "27261A"
    case Weathervane = "2B2517"
    case DraculaOrchid = "2C3337"
    case NightTurquoise = "013A33"
    case Onyx = "464544"
    case NecronCompound = "82898F"
    case ButtonEyes = "4E3B31"
    case YvesKleinBlue = "00008B"
    case GoldenCartridge = "BDB76B"
    case DarkCyan = "008B8B"
    case DarkEggplant = "452D35"
    case AmberShade = "FF7E00"
    case IridescentRed = "CC4E5C"
    case Gingerbread = "904D30"
    case JadeGravel = "0ABAB5"
    case PacificBlue = "1CA9C9"
    case RedTorii = "D53E07"
    case Tabasco = "A12312"
    case TomatoBruschetta = "FF6347"
    case ThymeAndSalt = "5DA130"
    case Cucumber = "006400"
    case RadicalGreen = "35682D"
    case YuzuJam = "FAD201"
    case BoldIrish = "308446"
    case RedRebellion = "CC0605"
    case RedStrawberrySpinach = "F54021"
    case PurpleMaximumRed = "A03472"
    case StormsMountain = "8D948D"
    case AzureDragon = "063971"
    case BlackDynamic = "1E1E1E"
    case MintJelly = "45CEA2"
    case GreenFlinders = "6C7156"
    case PinkKerrsPotato = "B57281"
    case PinkMud = "DDBEC3"
    case RoseMisty = "FFE4E1"
    case OrangeTangerine = "F28500"
    case RedOrchid = "AE848B"
    case GreyDim = "696969"
    case Pumpkin = "FF7518"
    case DarkNightSky = "20214F"
    case BlueTrust = "120A8F"
    case BurntUmber = "8A3324"
    case MediumTurquoise = "48D1CC"
    case GreenSeaMedium = "3CB371"
    case BrownMud = "5E490F"
    case Bruise = "803E75"
    case GreenPallid = "C0DCC0"
    case BlueAquarium = "66CDAA"
    case BlueMediumSlate = "7B68EE"
    case GreenMediumSpring = "00FA9A"
    case VegetableGarden = "8B8940"
    case BrownKinsusutake = "Browm"
    case GreenMoegi = "EE9374"
    case GreenKnoll = "657F4B"
    case YellowMineral = "D79D41"
    case Goldie = "C4A43D"
    case AfterStorm = "30626B"
    case GreenBlackPine = "386646"
    case BrownSearing = "673923"
    case Koi = "D35339"
    case RedBonBon = "8C4566"
    case Desirable = "AB343A"
    case GreenVerdun = "434B1B"
    case IcelandPoppy = "F7943C"
    case SolarFlare = "E8793E"
    case CranberrySauce = "A73853"
    case PinkBella = "E28090"
    case ComingStorme = "423C63"
    case MadderMagenta = "7F4870"
    case RedFlamingo  = "EE9086"
    case TaupeMedium = "674C47"
    case GreenPlantain = "2F6556"
    case StarSpangled = "395778"
    case VioletMediumRed = "C71585"
    case DamsonMauve = "543964"
    case MediumOrchid = "BA55D3"
    case RedFalu = "801818"
    case BerryBoost = "B55489"
    case Feldgrau = "4D5D53"
    case Pinkling = "EA8DF7"
    case VioletEggplant = "991199"
    case Pansy = "F75394"
    case VioletViolet = "8000FF"
    case Airforce = "354D73"
    case ManchesterNight = "324AB2"
    case PurpleClimax = "8B00FF"
    case VioletFog = "926EAE"
    case MilkCucumber = "BEF574"
    case RoseFrench = "F64A8A"
    case PhthaloGreen = "123524"
    case FuzzyWuzzy = "CC6666"
    case PurplePizzazz = "F754E1"
    case PurpleUrnOrchid = "C364C5"
    case TarnishedBrass = "806B2A"
    case GreenKelp = "2E3A23"
    case FrenchPuce = "4E1609"
    case BlueKashmire = "4D7198"
    case GreenClassic = "3CAA3C"
    case Lager = "F7F21A"
    case DeepForestial = "2A5C03"
    case YellowSchoolBus = "FFD800"
    case Tan = "D2B48C"
    case Rajah = "FAA76C"
    case Burnham = "1F4037"
    case HitPink = "FFA474"
    case BrownMedium = "834D18"
    case Emperor = "505050"
    case AzureRadiance = "008CF0"
    case SpringWood = "FFF5EE"
    case SurfGreen = "008080"
    case Bianca = "FFFAF0"
    case YellowPeach = "EEE6A3"
    case RedPersian = "CE2029"
    case ScotchMilk = "FFFDDF"
    case Raspberry = "E3256B"
    case PictonBlue = "1FAEE9"
    case LightNavyBlue = "3B5998"
    case DarkPastelBlue = "78A2B7"
    case ChathamsBlue = "1A4780"
    case ChinaIvory = "FCFCEE"
    case LightGoldenYellow = "FFF8DC"
    case BlueArctyClickSky = "00CCCC"
    case YellowMikado = "FFCC00"
    case FlaxSmoke = "7D8471"
    case BrightAqua = "00FFFF"
    case LimeYellow = "F8F32B"
    case Cashmere = "EBC2AF"
    case BubbleGum = "FFCBBB"
    case ShipGrey = "343E40"
    case DarkJungleGreen = "212121"
    case CocoaBean = "412227"
    case Dune = "3B3C36"
    case BleachedCedar = "23282B"
    case Gondola = "18171C"
    case GreenRangoon = "141613"
    case WoodBark = "1F0E11"
    case Cinder = "1D1018"
    case GreenRacing = "161A1E"
    case ClearBlack = "000000"
    case SmokyBlack = "0A0A0A"
    case Thistle = "D8BFD8"
    case LanguidLavender = "#EBC7DF"
    case GoldLuxor = "A08040"
    case Mojo = "CA3A27"
    case RedOrange = "ED4830"
    case GreenEmerald = "7FFF00"
    case BrightGold = "F5D033"
    case YellowLighting = "F4C430"
    case PinkBushful = "C54B8C"
    case PinkFuchsia = "FB7EFD"
    case BrownWoody = "45322E"
    case CocoaBrown = "D2691E"
    case BrownSorrel = "CDB891"
    case NeonPink = "FF33CC"
    case BlueElectric = "7DF9FF"
    case YellowNeon = "CCFF00"
    case LemonLime = "CEFF1D"
    case Viridian = "40826D"
    case ArtyClickAmber = "FFBF00"
    case GreenBarf = "93AA00"
    case OrangeBasketBall = "FF845C"
    case Supernova = "F4C800"
    case GreenLaSalle = "007D34"
    case OrangeyRed = "F13A13"
    case RichPurple = "7E0059"
    case RedVenetian = "C10020"
    case OrangeDark = "FF8E00"
    case OrangeBlade = "FF6800"
    case RedMediumViolet = "D5265B"
    case WarmPurple = "943391"
    case GreenElf = "00836E"
    case PinkTrendy = "8F509D"
    case BrightTurquoise = "08E8DE"
    case YellowishOrange = "FFB300"
    case GreenBrightLime = "66FF00"
    case PinkLightSalmon = "FFA089"
    case PinkShocking = "FC0FC0"
    case NiceBlue = "007CAD"
    case PinkBlush = "E0B0FF"
    case PurpleVibrant = "CD00CD"
}

public struct MyColors {
    
    var colorsDict =
    [
        "FBCEB1": "Абрикосовый",
        "FDD9B5": "Абрикосовый Крайола",
        "B5B8B1": "Агатовый серый",
        "7FFFD4": "Аквамариновый",
        "78DBE2": "Аквамариновый Крайола",
        "E32636": "Ализариновый красный",
        "FF2400": "Алый",
        "AB274F": "Амарантово-пурпурный",
        "F19CBB": "Амарантово-розовый",
        "E52B50": "Амарантовый",
        "9F2B68": "Амарантовый глубоко-пурпурный",
        "ED3CCA": "Амарантовый маджента",
        "CD2682": "Амарантовый светло-вишневый",
        "FF033E": "Американский розовый",
        "9966CC": "Аметистовый",
        "CD9575": "Античная латунь",
        "293133": "Антрацитово-серый",
        "464451": "Антрацитовый",
        "44944A": "Арлекин",
        "2F4F4F": "Аспидно-серый",
        "6A5ACD": "Аспидно-синий",
        "A8E4A0": "Бабушкины яблоки",
        "4E5754": "Базальтово-серый",
        "614051": "Баклажанный Крайола",
        "990066": "Баклажановый",
        "6E5160": "Баклажановый Крайола",
        "FAE7B5": "Бананомания",
        "CCCCFF": "Барвинок, перванш",
        "C5D0E6": "Барвинок Крайола",
        "FAEEDD": "Бедра испуганной нимфы",
        "79553D": "Бежево-коричневый",
        "C1876B": "Бежево-красный",
        "6D6552": "Бежево-серый",
        "F5F5DC": "Бежевый",
        "A5A5A5": "Бело-алюминиевый",
        "BDECB6": "Бело-зеленый",
        "FFFAFA": "Белоснежный",
        "FFFFFF": "Белый",
        "FAEBD7": "Белый антик",
        "FFDEAD": "Белый навахо",
        "003153": "Берлинская лазурь",
        "77DDE7": "Бирюзово-голубой Крайола",
        "1E5945": "Бирюзово-зеленый",
        "3F888F": "Бирюзово-синий",
        "30D5C8": "Бирюзовый",
        "FFE4C4": "Бисквитный",
        "A5260A": "Бисмарк-фуриозо",
        "3D2B1F": "Бистр",
        "ABCDEF": "Бледно-васильковый",
        "FFDB8B": "Бледно-желтый",
        "8D917A": "Бледно-зелено-серый",
        "89AC76": "Бледно-зеленый",
        "EEE8AA": "Бледно-золотистый",
        "B03F35": "Бледно-карминный",
        "DDADAF": "Бледно-каштановый",
        "755C48": "Бледно-коричневый",
        "DABDAB": "Бледно-песочный",
        "F984E5": "Бледно-пурпурный",
        "FFCBDB": "Бледно-розоватый",
        "FADADD": "Бледно-розовый",
        "AFEEEE": "Бледно-синий",
        "957B8D": "Бледно-фиолетовый",
        "ECEBBD": "Бледный весенний бутон",
        "F0D698": "Бледный желто-зеленый",
        "FFC8A8": "Бледный желто-розовый",
        "FFDF84": "Бледный зеленовато-желтый",
        "98FB98": "Бледный зеленый",
        "AC7580": "Бледный красно-пурпурный",
        "FFCA86": "Бледный оранжево-желтый",
        "FDBDBA": "Бледный пурпурно-розовый",
        "8A7F8E": "Бледный пурпурно-синий",
        "BC987E": "Бледный серо-коричневый",
        "919192": "Бледный синий",
        "D87093": "Бледный фиолетово-красный",
        "CED23A": "Блестящий желто-зеленый",
        "8CCB5E": "Блестящий желтовато-зеленый",
        "FFCF40": "Блестящий желтый",
        "FFDC33": "Блестящий зеленовато-желтый",
        "2A8D9C": "Блестящий зеленовато-синий",
        "47A76A": "Блестящий зеленый",
        "FFB841": "Блестящий оранжевый",
        "FF97BB": "Блестящий пурпурно-розовый",
        "62639B": "Блестящий пурпурно-синий",
        "DD80CC": "Блестящий пурпурный",
        "009B76": "Блестящий синевато-зеленый",
        "4285B4": "Блестящий синий",
        "755D9A": "Блестящий фиолетовый",
        "755A57": "Блошиный (Красновато-коричневый)",
        "9F8170": "Бобровый",
        "480607": "Болгарский розовый",
        "ACB78E": "Болотный",
        "B00000": "Бордо (Красно-бордовый)",
        "641C34": "Бордово-фиолетовый",
        "9B2D30": "Бордовый",
        "D5D5D5": "Бороды Абдель-Керима",
        "4C514A": "Брезентово-серый",
        "3E5F8A": "Бриллиантово-синий",
        "FFB02E": "Бриллиантовый оранжево-желтый",
        "CD7F32": "Бронзовый",
        "900020": "Бургундский",
        "45161C": "Бурый",
        "343B29": "Бутылочно-зеленый",
        "D5713F": "Ванильный",
        "6495ED": "Васильковый",
        "9ACEEB": "Васильковый Крайола",
        "DAD871": "Вердепешевый",
        "34C924": "Вердепомовый",
        "DE4C8A": "Вересково-фиолетовый",
        "00FF7F": "Весенне-зеленый (Зеленая весна)",
        "ECEABE": "Весенне-зеленый Крайола",
        "A7FC00": "Весенний бутон",
        "BD33A4": "Византийский",
        "702963": "Византия",
        "5E2129": "Винно-красный",
        "414833": "Винтовочный зеленый",
        "911E42": "Вишневый (Вишня)",
        "64400F": "В меру оливково-коричневый",
        "256D7B": "Водная синь",
        "0095B6": "Воды пляжа Бонди",
        "FFCF48": "Восход солнца",
        "B8B799": "Галечный серый",
        "DCDCDC": "Гейнсборо",
        "DF73FF": "Гелиотроп (Гелиотроповый)",
        "F3A505": "Георгиново-желтый",
        "734222": "Глиняный коричневый",
        "C9A0DC": "Глициния (Глициниевый)",
        "CDA4DE": "Глициния Крайола",
        "C154C1": "Глубокая фуксия Крайола",
        "593315": "Глубокий желто-коричневый",
        "F64A46": "Глубокий желто-розовый",
        "00541F": "Глубокий желтовато-зеленый",
        "B57900": "Глубокий желтый",
        "425E17": "Глубокий желтый зеленый",
        "9F8200": "Глубокий зеленовато-желтый",
        "004524": "Глубокий зеленый",
        "EF3038": "Глубокий карминно-розовый",
        "A9203E": "Глубокий карминный",
        "FF4040": "Глубокий коралловый",
        "4D220E": "Глубокий коричневый",
        "490005": "Глубокий красно-коричневый",
        "A91D11": "Глубокий красно-оранжевый",
        "641349": "Глубокий красно-пурпурный",
        "7B001C": "Глубокий красный",
        "142300": "Глубокий оливково-зеленый",
        "D76E00": "Глубокий оранжево-желтый",
        "C34D0A": "Глубокий оранжевый",
        "6F0035": "Глубокий пурпурно-красный",
        "EB5284": "Абрикосовый",
        "1A153F": "Абрикосовый Крайола",
        "531A50": "Агатовый серый",
        "FF1493": "Аквамариновый",
        "00382B": "Аквамариновый Крайола",
        "002F55": "Ализариновый красный",
        "240935": "Алый",
        "423189": "Амарантово-пурпурный",
        "606E8C": "Амарантово-розовый",
        "42AAFF": "Амарантовый",
        "00BFFF": "Амарантовый глубоко-пурпурный",
        "A2A2D0": "Амарантовый маджента",
        "80DAEB": "Амарантовый светло-вишневый",
        "0E294B": "Американский розовый",
        "30BA8F": "Аметистовый",
        "87CEEB": "Античная латунь",
        "FFDB58": "Антрацитово-серый",
        "FD7C6E": "Антрацитовый",
        "F34723": "Арлекин",
        "2F353B": "Аспидно-серый",
        "1C1C1C": "Аспидно-синий",
        "474A51": "Бабушкины яблоки",
        "C7D0CC": "Базальтово-серый",
        "D71868": "Баклажанный Крайола",
        "D1E231": "Баклажановый",
        "EFD334": "Баклажановый Крайола",
        "E49B0F": "Бананомания",
        "B2EC5D": "Барвинок, перванш",
        "00693E": "Барвинок Крайола",
        "CA3767": "Бедра испуганной нимфы",
        "1560BD": "Бежево-коричневый",
        "FF43A4": "Бежево-красный",
        "FC6C85": "Бежево-серый",
        "A2ADD0": "Бежевый",
        "F5F5F5": "Бело-алюминиевый",
        "F4A900": "Бело-зеленый",
        "FDBCB4": "Белоснежный",
        "434B4D": "Белый",
        "EDFF21": "Белый антик",
        "E1CC4F": "Белый навахо",
        "9ACD32": "Берлинская лазурь",
        "C5E384": "Бирюзово-голубой Крайола",
        "CDA434": "Бирюзово-зеленый",
        "47402E": "Бирюзово-синий",
        "ED760E": "Бирюзовый",
        "FFAE42": "Бисквитный",
        "FADFAD": "Бисмарк-фуриозо",
        "FFE4B2": "Бистр",
        "8F8B66": "Бледно-васильковый",
        "FFE2B7": "Бледно-желтый",
        "CAA885": "Бледно-зелено-серый",
        "FFFF00": "Бледно-зеленый",
        "9D9101": "Бледно-золотистый",
        "FCE883": "Бледно-карминный",
        "D6AE01": "Бледно-каштановый",
        "EAE6CA": "Бледно-коричневый",
        "CC5500": "Бледно-песочный",
        "CB6586": "Бледно-пурпурный",
        "1E90FF": "Бледно-розоватый",
        "78866B": "Бледно-розовый",
        "FF47CA": "Бледно-синий",
        "7CFC00": "Бледно-фиолетовый",
        "01796F": "Бледный весенний бутон",
        "158078": "Бледный желто-зеленый",
        "BEBD7F": "Бледный желто-розовый",
        "ADFF2F": "Бледный зеленовато-желтый",
        "F0E891": "Бледный зеленый",
        "826C34": "Бледный красно-пурпурный",
        "BFFF00": "Бледный оранжево-желтый",
        "4D5645": "Бледный пурпурно-розовый",
        "1F3438": "Бледный пурпурно-синий",
        "1164B4": "Бледный серо-коричневый",
        "F5E6CB": "Бледный синий",
        "7A7666": "Бледный фиолетово-красный",
        "181513": "Блестящий желто-зеленый",
        "4E5452": "Блестящий желтовато-зеленый",
        "2E8B57": "Блестящий желтый",
        "3BB08F": "Блестящий зеленовато-желтый",
        "29AB87": "Блестящий зеленовато-синий",
        "008000": "Блестящий зеленый",
        "1CAC78": "Блестящий оранжевый",
        "ADDFAD": "Блестящий пурпурно-розовый",
        "006633": "Блестящий пурпурно-синий",
        "2F4538": "Блестящий пурпурный",
        "004953": "Блестящий синевато-зеленый",
        "4F7942": "Блестящий синий",
        "009A63": "Блестящий фиолетовый",
        "D0F0C0": "Блошиный (Красновато-коричневый)",
        "FCD975": "Бобровый",
        "DAA520": "Болгарский розовый",
        "712F26": "Болотный",
        "FFD700": "Бордо (Красно-бордовый)",
        "E7C697": "Бордово-фиолетовый",
        "321414": "Бордовый",
        "79443B": "Бороды Абдель-Керима",
        "009B77": "Брезентово-серый",
        "287233": "Бриллиантово-синий",
        "50C878": "Бриллиантовый оранжево-желтый",
        "4B0082": "Бронзовый",
        "5D76CB": "Бургундский",
        "138808": "Бурый",
        "CD5C5C": "Бутылочно-зеленый",
        "4CBB17": "Ванильный",
        "BDDA57": "Васильковый",
        "5F9EA0": "Васильковый Крайола",
        "B0B7C6": "Вердепешевый",
        "A25F2A": "Вердепомовый",
        "8B8C7A": "Вересково-фиолетовый",
        "FFFF99": "Весенне-зеленый (Зеленая весна)",
        "1B5583": "Весенне-зеленый Крайола",
        "C41E3A": "Весенний бутон",
        "1CD3A2": "Византийский",
        "960018": "Византия",
        "A2231D": "Винно-красный",
        "EB4C42": "Винтовочный зеленый",
        "FF0033": "Вишневый (Вишня)",
        "633A34": "В меру оливково-коричневый",
        "BC5D58": "Водная синь",
        "99958C": "Воды пляжа Бонди",
        "6C6960": "Восход солнца",
        "FF4D00": "Галечный серый",
        "CB4154": "Гейнсборо",
        "884535": "Гелиотроп (Гелиотроповый)",
        "E34234": "Георгиново-желтый",
        "507D2A": "Глиняный коричневый",
        "D53032": "Глициния (Глициниевый)",
        "1E213D": "Глициния Крайола",
        "0047AB": "Глубокая фуксия Крайола",
        "F0DC82": "Глубокий желто-коричневый",
        "FFA000": "Глубокий желто-розовый",
        "B32821": "Глубокий желтовато-зеленый",
        "FF7F50": "Глубокий желтый",
        "893F45": "Глубокий желтый зеленый",
        "B15124": "Глубокий зеленовато-желтый",
        "CD9A7B": "Глубокий зеленый",
        "503D33": "Глубокий карминно-розовый",
        "140F0B": "Глубокий карминный",
        "8A6642": "Глубокий коралловый",
        "A52A2A": "Глубокий коричневый",
        "C19A6B": "Глубокий красно-коричневый",
        "39352A": "Глубокий красно-оранжевый",
        "781F19": "Глубокий красно-пурпурный",
        "800000": "Глубокий красный",
        "C8385A": "Глубокий оливково-зеленый",
        "25221B": "Глубокий оранжево-желтый",
        "964B00": "Глубокий оранжевый",
        "B4674D": "Глубокий пурпурно-красный",
        "464531": "Абрикосовый",
        "8B4513": "Абрикосовый Крайола",
        "7B3F00": "Агатовый серый",
        "CA2C92": "Аквамариновый",
        "7851A9": "Аквамариновый Крайола",
        "4169E1": "Ализариновый красный",
        "FFF8E7": "Алый",
        "414A4C": "Амарантово-пурпурный",
        "442D25": "Амарантово-розовый",
        "FDD5B1": "Амарантовый",
        "CD5700": "Амарантовый глубоко-пурпурный",
        "80461B": "Амарантовый маджента",
        "592321": "Амарантовый светло-вишневый",
        "C93C20": "Американский розовый",
        "FF5349": "Аметистовый",
        "6D3F5B": "Античная латунь",
        "922B3E": "Антрацитово-серый",
        "C0448F": "Антрацитовый",
        "8B6C62": "Арлекин",
        "1E1112": "Аспидно-серый",
        "C04000": "Аспидно-синий",
        "CD4A4C": "Бабушкины яблоки",
        "FF0000": "Базальтово-серый",
        "EE204D": "Баклажанный Крайола",
        "F4A460": "Баклажановый",
        "FFFDD0": "Баклажановый Крайола",
        "FDF4E3": "Бананомания",
        "C3B091": "Барвинок, перванш",
        "76FF7A": "Барвинок Крайола",
        "FB607F": "Бедра испуганной нимфы",
        "E4A010": "Бежево-коричневый",
        "FBEC5D": "Бежево-красный",
        "E6E6FA": "Бежево-серый",
        "FCB4D5": "Бежевый",
        "FBA0E3": "Бело-алюминиевый",
        "FEFE22": "Бело-зеленый",
        "007BA7": "Белоснежный",
        "025669": "Белый",
        "007FFF": "Белый антик",
        "1DACD6": "Белый навахо",
        "00FF00": "Берлинская лазурь",
        "32CD32": "Бирюзово-голубой Крайола",
        "979AAA": "Бирюзово-зеленый",
        "B5A642": "Бирюзово-синий",
        "DBD7D2": "Бирюзовый",
        "228B22": "Бисквитный",
        "534B4F": "Бисмарк-фуриозо",
        "DB7093": "Бистр",
        "C7B446": "Бледно-васильковый",
        "FFF44F": "Бледно-желтый",
        "FFFACD": "Бледно-зелено-серый",
        "FDE910": "Бледно-зеленый",
        "2D572C": "Бледно-золотистый",
        "6DAE81": "Бледно-карминный",
        "D95030": "Бледно-каштановый",
        "E55137": "Бледно-коричневый",
        "FF8C69": "Бледно-песочный",
        "FF9BAA": "Бледно-пурпурный",
        "FAF0E6": "Бледно-розоватый",
        "F80000": "Бледно-розовый",
        "FFA420": "Бледно-синий",
        "7B917B": "Бледно-фиолетовый",
        "AAF0D1": "Бледный весенний бутон",
        "F8F4FF": "Бледный желто-зеленый",
        "FF00FF": "Бледный желто-розовый",
        "F664AF": "Бледный зеленовато-желтый",
        "EDD19C": "Бледный зеленый",
        "4C9141": "Бледный красно-пурпурный",
        "FFBD88": "Бледный оранжево-желтый",
        "0BDA51": "Бледный пурпурно-розовый",
        "C51D34": "Бледный пурпурно-синий",
        "B3446C": "Бледный серо-коричневый",
        "DC143C": "Бледный синий",
        "993366": "Бледный фиолетово-красный",
        "FF8243": "Блестящий желто-зеленый",
        "E1523D": "Блестящий желтовато-зеленый",
        "FF8800": "Блестящий желтый",
        "4C5866": "Блестящий зеленовато-желтый",
        "AD655F": "Блестящий зеленовато-синий",
        "4C2F27": "Блестящий зеленый",
        "8E402A": "Блестящий оранжевый",
        "996666": "Блестящий пурпурно-розовый",
        "B87333": "Блестящий пурпурно-синий",
        "DD9475": "Блестящий пурпурный",
        "F0FFF0": "Блестящий синевато-зеленый",
        "A98307": "Блестящий синий",
        "FEE5AC": "Блестящий фиолетовый",
        "FF4F00": "Блошиный (Красновато-коричневый)",
        "00416A": "Бобровый",
        "EFDECD": "Болгарский розовый",
        "21421E": "Болотный",
        "EF0097": "Бордо (Красно-бордовый)",
        "F400A1": "Бордово-фиолетовый",
        "FFE4B5": "Бордовый",
        "17806D": "Бороды Абдель-Керима",
        "F36223": "Брезентово-серый",
        "54FF9F": "Бриллиантово-синий",
        "9FE2BF": "Бриллиантовый оранжево-желтый",
        "1C6B72": "Бронзовый",
        "009900": "Бургундский",
        "646B63": "Бурый",
        "98FF98": "Бутылочно-зеленый",
        "497E76": "Ванильный",
        "20603D": "Васильковый",
        "F5FFFA": "Васильковый Крайола",
        "3EB489": "Вердепешевый",
        "DC9D00": "Вердепомовый",
        "7F8F18": "Вересково-фиолетовый",
        "95500C": "Весенне-зеленый (Зеленая весна)",
        "FF7A5C": "Весенне-зеленый Крайола",
        "478430": "Весенний бутон",
        "E59E1F": "Византийский",
        "CCA817": "Византия",
        "00677E": "Винно-красный",
        "006B3C": "Винтовочный зеленый",
        "753313": "Вишневый (Вишня)",
        "7F180D": "В меру оливково-коричневый",
        "FFB961": "Водная синь",
        "9A366B": "Воды пляжа Бонди",
        "BF2233": "Восход солнца",
        "0A4500": "Галечный серый",
        "FF8E0D": "Гейнсборо",
        "EC7C26": "Гелиотроп (Гелиотроповый)",
        "B32851": "Георгиново-желтый",
        "F6768E": "Глиняный коричневый",
        "474389": "Глициния (Глициниевый)",
        "FD7B7C": "Глициния Крайола",
        "006D5B": "Глубокая фуксия Крайола",
        "00538A": "Глубокий желто-коричневый",
        "53377A": "Глубокий желто-розовый",
        "734A12": "Глубокий желтовато-зеленый",
        "F0FFFF": "Глубокий желтый",
        "2271B3": "Глубокий желтый зеленый",
        "7FC7FF": "Глубокий зеленовато-желтый",
        "6B8E23": "Глубокий зеленый",
        "FFFF66": "Глубокий карминно-розовый",
        "FFA343": "Глубокий карминный",
        "00A86B": "Глубокий коралловый",
        "9DB1CC": "Глубокий коричневый",
        "252850": "Глубокий красно-коричневый",
        "C9DC87": "Глубокий красно-оранжевый",
        "EA7E5D": "Глубокий красно-пурпурный",
        "AF2B1E": "Глубокий красный",
        "FF7F49": "Глубокий оливково-зеленый",
        "FDDB6D": "Глубокий оранжево-желтый",
        "1D334A": "Глубокий оранжевый",
        "642424": "Глубокий пурпурно-красный",
        "59351F": "Абрикосовый",
        "999950": "Абрикосовый Крайола",
        "424632": "Агатовый серый",
        "BAB86C": "Аквамариновый",
        "6F4F28": "Аквамариновый Крайола",
        "121910": "Ализариновый красный",
        "808000": "Алый",
        "4D4234": "Амарантово-пурпурный",
        "015D52": "Амарантово-розовый",
        "B784A7": "Амарантовый",
        "FD5E53": "Амарантовый глубоко-пурпурный",
        "F8D568": "Амарантовый маджента",
        "A65E2E": "Амарантовый светло-вишневый",
        "FF2B2B": "Американский розовый",
        "FFCC99": "Аметистовый",
        "FF9966": "Античная латунь",
        "FFA500": "Антрацитово-серый",
        "FF7538": "Антрацитовый",
        "5B3A29": "Арлекин",
        "B32428": "Аспидно-серый",
        "DA70D6": "Аспидно-синий",
        "E6A8D7": "Бабушкины яблоки",
        "FFBA00": "Базальтово-серый",
        "49678D": "Баклажанный Крайола",
        "355E3B": "Баклажановый",
        "CC7722": "Баклажановый Крайола",
        "AEA04B": "Бананомания",
        "955F20": "Барвинок, перванш",
        "E6BBC1": "Барвинок Крайола",
        "D8DEBA": "Бедра испуганной нимфы",
        "CBBAC5": "Бежево-коричневый",
        "C1CACA": "Бежево-красный",
        "D8B1BF": "Бежево-серый",
        "002800": "Бежевый",
        "470736": "Бело-алюминиевый",
        "4F0014": "Бело-зеленый",
        "470027": "Белоснежный",
        "320B35": "Белый",
        "E3A9BE": "Белый антик",
        "C6DF90": "Белый навахо",
        "A3C6C0": "Берлинская лазурь",
        "98C793": "Бирюзово-голубой Крайола",
        "BAACC7": "Бирюзово-зеленый",
        "A0D6B4": "Бирюзово-синий",
        "A6BDD7": "Бирюзовый",
        "EEBEF1": "Бисквитный",
        "230D21": "Бисмарк-фуриозо",
        "560319": "Бистр",
        "132712": "Бледно-васильковый",
        "022027": "Бледно-желтый",
        "16251C": "Бледно-зелено-серый",
        "270A1F": "Бледно-зеленый",
        "320A18": "Бледно-золотистый",
        "362C12": "Бледно-карминный",
        "28071A": "Бледно-каштановый",
        "001D18": "Бледно-коричневый",
        "4C3C18": "Бледно-песочный",
        "FFEBCD": "Бледно-пурпурный",
        "C7FCEC": "Бледно-розоватый",
        "71BC78": "Бледно-розовый",
        "3D642D": "Бледно-синий",
        "7FB5B5": "Бледно-фиолетовый",
        "EFA94A": "Бледный весенний бутон",
        "77DD77": "Бледный желто-зеленый",
        "FF7514": "Бледный желто-розовый",
        "FFD1DC": "Бледный зеленовато-желтый",
        "5D9B9B": "Бледный зеленый",
        "A18594": "Бледный красно-пурпурный",
        "316650": "Бледный оранжево-желтый",
        "DEAA88": "Бледный пурпурно-розовый",
        "6A5D4D": "Бледный пурпурно-синий",
        "6C6874": "Бледный серо-коричневый",
        "1C542D": "Бледный синий",
        "705335": "Бледный фиолетово-красный",
        "C35831": "Блестящий желто-зеленый",
        "B44C43": "Блестящий желтовато-зеленый",
        "721422": "Блестящий желтый",
        "8673A1": "Блестящий зеленовато-желтый",
        "2A6478": "Блестящий зеленовато-синий",
        "763C28": "Блестящий зеленый",
        "898176": "Блестящий оранжевый",
        "102C54": "Блестящий пурпурно-розовый",
        "193737": "Блестящий пурпурно-синий",
        "9C9C9C": "Блестящий пурпурный",
        "828282": "Блестящий синевато-зеленый",
        "00A693": "Блестящий синий",
        "32127A": "Блестящий фиолетовый",
        "CC3333": "Блошиный (Красновато-коричневый)",
        "FE28A2": "Бобровый",
        "6600FF": "Болгарский розовый",
        "FFE5B4": "Болотный",
        "FFCFAB": "Бордо (Красно-бордовый)",
        "CD853F": "Бордово-фиолетовый",
        "EFCDB8": "Бордовый",
        "C6A664": "Бороды Абдель-Керима",
        "FCDD76": "Брезентово-серый",
        "967117": "Бриллиантово-синий",
        "00A550": "Бриллиантовый оранжево-желтый",
        "31372B": "Бронзовый",
        "F8173E": "Бургундский",
        "7F7679": "Бурый",
        "FFEFD5": "Бутылочно-зеленый",
        "8A795D": "Ванильный",
        "003366": "Васильковый",
        "1A4876": "Васильковый Крайола",
        "191970": "Вердепешевый",
        "FF9218": "Вердепомовый",
        "131313": "Вересково-фиолетовый",
        "F8F8FF": "Весенне-зеленый (Зеленая весна)",
        "FF00CC": "Весенне-зеленый Крайола",
        "FADBC8": "Весенний бутон",
        "75151E": "Византийский",
        "88706B": "Византия",
        "20155E": "Винно-красный",
        "4A192C": "Винтовочный зеленый",
        "1B1116": "Вишневый (Вишня)",
        "9D81BA": "В меру оливково-коричневый",
        "7442C8": "Водная синь",
        "800080": "Воды пляжа Бонди",
        "F5DEB3": "Восход солнца",
        "F75E25": "Галечный серый",
        "FF7E93": "Гейнсборо",
        "7D7F7D": "Гелиотроп (Гелиотроповый)",
        "B0E0E6": "Георгиново-желтый",
        "CC8899": "Глиняный коричневый",
        "FF496C": "Глициния (Глициниевый)",
        "F3DA0B": "Глициния Крайола",
        "587246": "Глубокая фуксия Крайола",
        "B7410E": "Глубокий желто-коричневый",
        "EF98AA": "Глубокий желто-розовый",
        "C8A696": "Глубокий желтовато-зеленый",
        "FFAACC": "Глубокий желтый",
        "AB4E52": "Глубокий желтый зеленый",
        "FF77FF": "Глубокий зеленовато-желтый",
        "B76E79": "Глубокий зеленый",
        "BC8F8F": "Глубокий карминно-розовый",
        "FFF0F5": "Глубокий карминный",
        "905D5D": "Глубокий коралловый",
        "EE82EE": "Глубокий коричневый",
        "674846": "Глубокий красно-коричневый",
        "FFC0CB": "Глубокий красно-оранжевый",
        "FC89AC": "Глубокий красно-пурпурный",
        "D36E70": "Глубокий красный",
        "AA98A9": "Глубокий оливково-зеленый",
        "65000B": "Глубокий оранжево-желтый",
        "997A8D": "Глубокий оранжевый",
        "FDDDE6": "Глубокий пурпурно-красный",
        "FC74FD": "Абрикосовый",
        "F78FA7": "Абрикосовый Крайола",
        "9B111E": "Агатовый серый",
        "DE5D83": "Аквамариновый",
        "D77D31": "Аквамариновый Крайола",
        "99FF99": "Ализариновый красный",
        "92000A": "Алый",
        "1D1E33": "Амарантово-пурпурный",
        "082567": "Амарантово-розовый",
        "DE3163": "Амарантовый",
        "DCD0FF": "Амарантовый глубоко-пурпурный",
        "E28B00": "Амарантовый маджента",
        "DDA0DD": "Амарантовый светло-вишневый",
        "E6D690": "Американский розовый",
        "40E0D0": "Аметистовый",
        "DD4492": "Античная латунь",
        "87CEFA": "Антрацитово-серый",
        "FFFFE0": "Антрацитовый",
        "FAFAD2": "Арлекин",
        "90EE90": "Аспидно-серый",
        "FFEC8B": "Аспидно-синий",
        "FFBCAD": "Бабушкины яблоки",
        "987654": "Базальтово-серый",
        "ED9121": "Баклажанный Крайола",
        "846A20": "Баклажановый",
        "FDEAA8": "Баклажановый Крайола",
        "BA7FA2": "Бананомания",
        "F984EF": "Барвинок, перванш",
        "FFB6C1": "Барвинок Крайола",
        "C9C0BB": "Бедра испуганной нимфы",
        "BBBBBB": "Бежево-коричневый",
        "A6CAF0": "Бежево-красный",
        "D84B20": "Бежево-серый",
        "876C99": "Бежевый",
        "20B2AA": "Бело-алюминиевый",
        "778899": "Бело-зеленый",
        "FFD35F": "Белоснежный",
        "2B6CC4": "Белый",
        "DCD36A": "Белый антик",
        "BB8B54": "Белый навахо",
        "FFB28B": "Берлинская лазурь",
        "BAAF96": "Бирюзово-голубой Крайола",
        "FFDE5A": "Бирюзово-зеленый",
        "649A9E": "Бирюзово-синий",
        "E66761": "Бирюзовый",
        "8B6D5C": "Бисквитный",
        "A86540": "Бисмарк-фуриозо",
        "AA6651": "Бистр",
        "BB6C8A": "Бледно-васильковый",
        "E63244": "Бледно-желтый",
        "945D0B": "Бледно-зелено-серый",
        "887359": "Бледно-зеленый",
        "FFA161": "Бледно-золотистый",
        "FFA8AF": "Бледно-карминный",
        "C8A99E": "Бледно-каштановый",
        "837DA2": "Бледно-коричневый",
        "EA899A": "Бледно-песочный",
        "B48764": "Бледно-пурпурный",
        "946B54": "Бледно-розоватый",
        "966A57": "Бледно-розовый",
        "B17267": "Бледно-синий",
        "8B734B": "Бледно-фиолетовый",
        "B27070": "Бледный весенний бутон",
        "84C3BE": "Бледный желто-зеленый",
        "D7D7D7": "Бледный желто-розовый",
        "6C92AF": "Бледный зеленовато-желтый",
        "669E85": "Бледный зеленый",
        "BEADA1": "Бледный красно-пурпурный",
        "ADD8E6": "Бледный оранжево-желтый",
        "B0C4DE": "Бледный пурпурно-розовый",
        "D0D0D0": "Бледный пурпурно-синий",
        "F0E68C": "Бледный серо-коричневый",
        "E0FFFF": "Бледный синий",
        "ACE1AF": "Бледный фиолетово-красный",
        "704214": "Блестящий желто-зеленый",
        "382C1E": "Блестящий желтовато-зеленый",
        "A5694F": "Блестящий желтый",
        "78858B": "Блестящий зеленовато-желтый",
        "465945": "Блестящий зеленовато-синий",
        "332F2C": "Блестящий зеленый",
        "8A9597": "Блестящий оранжевый",
        "C0C0C0": "Блестящий пурпурно-розовый",
        "CDC5C2": "Блестящий пурпурно-синий",
        "9E9764": "Блестящий пурпурный",
        "735184": "Блестящий синевато-зеленый",
        "90845B": "Блестящий синий",
        "785840": "Блестящий фиолетовый",
        "D39B85": "Блошиный (Красновато-коричневый)",
        "CEA262": "Бобровый",
        "C4A55F": "Болгарский розовый",
        "575E4E": "Болотный",
        "5A3D30": "Бордо (Красно-бордовый)",
        "5E3830": "Бордово-фиолетовый",
        "B85D43": "Бордовый",
        "7D4D5D": "Бороды Абдель-Керима",
        "8C4743": "Брезентово-серый",
        "52442C": "Бриллиантово-синий",
        "C2A894": "Бриллиантовый оранжево-желтый",
        "8C4852": "Бронзовый",
        "CC9293": "Бургундский",
        "413D51": "Бурый",
        "72525C": "Бутылочно-зеленый",
        "CF9B8F": "Ванильный",
        "4A545C": "Васильковый",
        "46394B": "Васильковый Крайола",
        "48442D": "Вердепешевый",
        "9DA1AA": "Вердепомовый",
        "808080": "Вересково-фиолетовый",
        "686C5E": "Весенне-зеленый (Зеленая весна)",
        "CADABA": "Весенне-зеленый Крайола",
        "403A3A": "Весенний бутон",
        "95918C": "Византийский",
        "6C7059": "Византия",
        "A0A0A4": "Винно-красный",
        "3E3B32": "Винтовочный зеленый",
        "26252D": "Вишневый (Вишня)",
        "6A5F31": "В меру оливково-коричневый",
        "CAC4B0": "Водная синь",
        "708090": "Воды пляжа Бонди",
        "E5BE01": "Восход солнца",
        "317F43": "Галечный серый",
        "6C3B2A": "Гейнсборо",
        "A52019": "Гелиотроп (Гелиотроповый)",
        "FF9900": "Георгиново-желтый",
        "969992": "Глиняный коричневый",
        "1E2460": "Глициния (Глициниевый)",
        "924E7D": "Глициния Крайола",
        "282828": "Глубокая фуксия Крайола",
        "A0522D": "Глубокий желто-коричневый",
        "E97451": "Глубокий желто-розовый",
        "79A0C1": "Глубокий желтовато-зеленый",
        "1F3A3D": "Глубокий желтый",
        "0D98BA": "Глубокий желтый зеленый",
        "8A2BE2": "Глубокий зеленовато-желтый",
        "6699CC": "Глубокий зеленый",
        "6C4675": "Глубокий карминно-розовый",
        "7366BD": "Глубокий карминный",
        "F9DFCF": "Глубокий коралловый",
        "7D746D": "Глубокий коричневый",
        "151719": "Глубокий красно-коричневый",
        "0000FF": "Глубокий красно-оранжевый",
        "AFDAFC": "Глубокий красно-пурпурный",
        "007DFF": "Глубокий красный",
        "3A75C4": "Глубокий оливково-зеленый",
        "1F75FE": "Глубокий оранжево-желтый",
        "474B4E": "Глубокий оранжевый",
        "1FCECB": "Глубокий пурпурно-красный",
        "18A7B5": "Абрикосовый",
        "122FAA": "Абрикосовый Крайола",
        "2A52BE": "Агатовый серый",
        "003399": "Аквамариновый",
        "4682B4": "Аквамариновый Крайола",
        "F0F8FF": "Ализариновый красный",
        "C8A2C8": "Алый",
        "B565A7": "Амарантово-пурпурный",
        "FF6E4A": "Амарантово-розовый",
        "FC2847": "Амарантовый",
        "FFBCD9": "Амарантовый глубоко-пурпурный",
        "434750": "Амарантовый маджента",
        "660066": "Амарантовый светло-вишневый",
        "8E4585": "Американский розовый",
        "F2DDC6": "Аметистовый",
        "F2E8C9": "Античная латунь",
        "FFFFF0": "Антрацитово-серый",
        "ACE5EE": "Антрацитовый",
        "F39F18": "Арлекин",
        "EFAF8C": "Аспидно-серый",
        "2C5545": "Аспидно-синий",
        "B94E48": "Бабушкины яблоки",
        "7BA05B": "Базальтово-серый",
        "87A96B": "Баклажанный Крайола",
        "AF4035": "Баклажановый",
        "0067A5": "Баклажановый Крайола",
        "9370D8": "Бананомания",
        "817066": "Барвинок, перванш",
        "231A24": "Барвинок Крайола",
        "C08081": "Бедра испуганной нимфы",
        "CFB53B": "Бежево-коричневый",
        "FDF5E6": "Бежево-красный",
        "EEDC82": "Бежево-серый",
        "D68A59": "Бежевый",
        "714B23": "Бело-алюминиевый",
        "909090": "Бело-зеленый",
        "CF3476": "Белоснежный",
        "5D3954": "Белый",
        "9932CC": "Белый антик",
        "CB2821": "Белый навахо",
        "116062": "Берлинская лазурь",
        "3B83BD": "Бирюзово-голубой Крайола",
        "D8A903": "Бирюзово-зеленый",
        "B07D2B": "Бирюзово-синий",
        "013220": "Бирюзовый",
        "986960": "Бисквитный",
        "CD5B45": "Бисмарк-фуриозо",
        "654321": "Бистр",
        "8B0000": "Бледно-васильковый",
        "08457E": "Бледно-желтый",
        "E9967A": "Бледно-зелено-серый",
        "FFA812": "Бледно-зеленый",
        "556832": "Бледно-золотистый",
        "FF8C00": "Бледно-карминный",
        "FFDAB9": "Бледно-каштановый",
        "472A3F": "Бледно-коричневый",
        "E75480": "Бледно-песочный",
        "915F6D": "Бледно-пурпурный",
        "483C32": "Бледно-розоватый",
        "49423D": "Бледно-розовый",
        "002137": "Бледно-синий",
        "000080": "Бледно-фиолетовый",
        "1974D2": "Бледный весенний бутон",
        "9400D3": "Бледный желто-зеленый",
        "8FBC8F": "Бледный желто-розовый",
        "483D8B": "Бледный зеленовато-желтый",
        "177245": "Бледный зеленый",
        "57A639": "Бледный красно-пурпурный",
        "918151": "Бледный оранжево-желтый",
        "CC6C5C": "Бледный пурпурно-розовый",
        "304B26": "Бледный пурпурно-синий",
        "3F2512": "Бледный серо-коричневый",
        "313830": "Бледный синий",
        "9B8127": "Бледный фиолетово-красный",
        "45433B": "Блестящий желто-зеленый",
        "003841": "Блестящий желтовато-зеленый",
        "203A27": "Блестящий желтый",
        "BADBAD": "Блестящий зеленовато-желтый",
        "B8860B": "Блестящий зеленовато-синий",
        "310062": "Блестящий зеленый",
        "35170C": "Блестящий оранжевый",
        "321011": "Блестящий пурпурно-розовый",
        "9B2F1F": "Блестящий пурпурно-синий",
        "4F273A": "Блестящий пурпурный",
        "523C36": "Блестящий синевато-зеленый",
        "681C23": "Блестящий синий",
        "8B008B": "Блестящий фиолетовый",
        "EA7500": "Блошиный (Красновато-коричневый)",
        "232C16": "Бобровый",
        "302112": "Болгарский розовый",
        "C37629": "Болотный",
        "03C03C": "Бордо (Красно-бордовый)",
        "5B1E31": "Бордово-фиолетовый",
        "C76574": "Бордовый",
        "564042": "Бороды Абдель-Керима",
        "1A162A": "Брезентово-серый",
        "660099": "Бриллиантово-синий",
        "C76864": "Бриллиантовый оранжево-желтый",
        "A47C45": "Бронзовый",
        "32221A": "Бургундский",
        "371F1C": "Бурый",
        "482A2A": "Бутылочно-зеленый",
        "27261A": "Ванильный",
        "2B2517": "Васильковый",
        "2C3337": "Васильковый Крайола",
        "013A33": "Вердепешевый",
        "464544": "Вердепомовый",
        "82898F": "Вересково-фиолетовый",
        "4E3B31": "Весенне-зеленый (Зеленая весна)",
        "00008B": "Весенне-зеленый Крайола",
        "BDB76B": "Весенний бутон",
        "008B8B": "Византийский",
        "452D35": "Византия",
        "FF7E00": "Винно-красный",
        "CC4E5C": "Винтовочный зеленый",
        "904D30": "Вишневый (Вишня)",
        "0ABAB5": "В меру оливково-коричневый",
        "1CA9C9": "Водная синь",
        "D53E07": "Воды пляжа Бонди",
        "A12312": "Восход солнца",
        "FF6347": "Галечный серый",
        "5DA130": "Гейнсборо",
        "006400": "Гелиотроп (Гелиотроповый)",
        "35682D": "Георгиново-желтый",
        "FAD201": "Глиняный коричневый",
        "308446": "Глициния (Глициниевый)",
        "CC0605": "Глициния Крайола",
        "F54021": "Глубокая фуксия Крайола",
        "A03472": "Глубокий желто-коричневый",
        "8D948D": "Глубокий желто-розовый",
        "063971": "Глубокий желтовато-зеленый",
        "1E1E1E": "Глубокий желтый",
        "45CEA2": "Глубокий желтый зеленый",
        "6C7156": "Глубокий зеленовато-желтый",
        "B57281": "Глубокий зеленый",
        "DDBEC3": "Глубокий карминно-розовый",
        "FFE4E1": "Глубокий карминный",
        "F28500": "Глубокий коралловый",
        "AE848B": "Глубокий коричневый",
        "696969": "Глубокий красно-коричневый",
        "FF7518": "Глубокий красно-оранжевый",
        "20214F": "Глубокий красно-пурпурный",
        "120A8F": "Глубокий красный",
        "8A3324": "Глубокий оливково-зеленый",
        "48D1CC": "Глубокий оранжево-желтый",
        "3CB371": "Глубокий оранжевый",
        "5E490F": "Глубокий пурпурно-красный",
        "803E75": "Абрикосовый",
        "C0DCC0": "Абрикосовый Крайола",
        "66CDAA": "Агатовый серый",
        "7B68EE": "Аквамариновый",
        "00FA9A": "Аквамариновый Крайола",
        "8B8940": "Ализариновый красный",
        "7D512D": "Алый",
        "EE9374": "Амарантово-пурпурный",
        "657F4B": "Амарантово-розовый",
        "D79D41": "Амарантовый",
        "C4A43D": "Амарантовый глубоко-пурпурный",
        "30626B": "Амарантовый маджента",
        "386646": "Амарантовый светло-вишневый",
        "673923": "Американский розовый",
        "D35339": "Аметистовый",
        "8C4566": "Античная латунь",
        "AB343A": "Антрацитово-серый",
        "434B1B": "Антрацитовый",
        "F7943C": "Арлекин",
        "E8793E": "Аспидно-серый",
        "A73853": "Аспидно-синий",
        "E28090": "Бабушкины яблоки",
        "423C63": "Базальтово-серый",
        "7F4870": "Баклажанный Крайола",
        "EE9086": "Баклажановый",
        "674C47": "Баклажановый Крайола",
        "2F6556": "Бананомания",
        "395778": "Барвинок, перванш",
        "C71585": "Барвинок Крайола",
        "543964": "Бедра испуганной нимфы",
        "BA55D3": "Бежево-коричневый",
        "801818": "Бежево-красный",
        "B55489": "Бежево-серый",
        "4D5D53": "Бежевый",
        "EA8DF7": "Бело-алюминиевый",
        "991199": "Бело-зеленый",
        "F75394": "Белоснежный",
        "8000FF": "Белый",
        "354D73": "Белый антик",
        "324AB2": "Белый навахо",
        "8B00FF": "Берлинская лазурь",
        "926EAE": "Бирюзово-голубой Крайола",
        "BEF574": "Бирюзово-зеленый",
        "F64A8A": "Бирюзово-синий",
        "123524": "Бирюзовый",
        "CC6666": "Бисквитный",
        "F754E1": "Бисмарк-фуриозо",
        "C364C5": "Бистр",
        "806B2A": "Бледно-васильковый",
        "2E3A23": "Бледно-желтый",
        "4E1609": "Бледно-зелено-серый",
        "4D7198": "Бледно-зеленый",
        "3CAA3C": "Бледно-золотистый",
        "F7F21A": "Бледно-карминный",
        "2A5C03": "Бледно-каштановый",
        "FFD800": "Бледно-коричневый",
        "D2B48C": "Бледно-песочный",
        "FAA76C": "Бледно-пурпурный",
        "1F4037": "Бледно-розоватый",
        "FFA474": "Бледно-розовый",
        "834D18": "Бледно-синий",
        "505050": "Бледно-фиолетовый",
        "008CF0": "Бледный весенний бутон",
        "FFF5EE": "Бледный желто-зеленый",
        "008080": "Бледный желто-розовый",
        "FFFAF0": "Бледный зеленовато-желтый",
        "EEE6A3": "Бледный зеленый",
        "CE2029": "Бледный красно-пурпурный",
        "FFFDDF": "Бледный оранжево-желтый",
        "E3256B": "Бледный пурпурно-розовый",
        "1FAEE9": "Бледный пурпурно-синий",
        "3B5998": "Бледный серо-коричневый",
        "78A2B7": "Бледный синий",
        "1A4780": "Бледный фиолетово-красный",
        "FCFCEE": "Блестящий желто-зеленый",
        "FFF8DC": "Блестящий желтовато-зеленый",
        "00CCCC": "Блестящий желтый",
        "FFCC00": "Блестящий зеленовато-желтый",
        "7D8471": "Блестящий зеленовато-синий",
        "00FFFF": "Блестящий зеленый",
        "F8F32B": "Блестящий оранжевый",
        "EBC2AF": "Блестящий пурпурно-розовый",
        "FFCBBB": "Блестящий пурпурно-синий",
        "343E40": "Блестящий пурпурный",
        "212121": "Блестящий синевато-зеленый",
        "412227": "Блестящий синий",
        "3B3C36": "Блестящий фиолетовый",
        "23282B": "Блошиный (Красновато-коричневый)",
        "18171C": "Бобровый",
        "141613": "Болгарский розовый",
        "1F0E11": "Болотный",
        "1D1018": "Бордо (Красно-бордовый)",
        "161A1E": "Бордово-фиолетовый",
        "000000": "Бордовый",
        "0A0A0A": "Бороды Абдель-Керима",
        "D8BFD8": "Брезентово-серый",
        "EBC7DF": "Бриллиантово-синий",
        "A08040": "Бриллиантовый оранжево-желтый",
        "CA3A27": "Бронзовый",
        "ED4830": "Бургундский",
        "7FFF00": "Бурый",
        "F5D033": "Бутылочно-зеленый",
        "F4C430": "Ванильный",
        "C54B8C": "Васильковый",
        "FB7EFD": "Васильковый Крайола",
        "45322E": "Вердепешевый",
        "D2691E": "Вердепомовый",
        "CDB891": "Вересково-фиолетовый",
        "FF33CC": "Весенне-зеленый (Зеленая весна)",
        "7DF9FF": "Весенне-зеленый Крайола",
        "CCFF00": "Весенний бутон",
        "CEFF1D": "Византийский",
        "40826D": "Византия",
        "FFBF00": "Винно-красный",
        "93AA00": "Винтовочный зеленый",
        "FF845C": "Вишневый (Вишня)",
        "F4C800": "В меру оливково-коричневый",
        "007D34": "Водная синь",
        "F13A13": "Воды пляжа Бонди",
        "7E0059": "Восход солнца",
        "C10020": "Галечный серый",
        "FF8E00": "Гейнсборо",
        "FF6800": "Гелиотроп (Гелиотроповый)",
        "D5265B": "Георгиново-желтый",
        "943391": "Глиняный коричневый",
        "00836E": "Глициния (Глициниевый)",
        "8F509D": "Глициния Крайола",
        "08E8DE": "Глубокая фуксия Крайола",
        "FFB300": "Глубокий желто-коричневый",
        "66FF00": "Глубокий желто-розовый",
        "FFA089": "Глубокий желтовато-зеленый",
        "FC0FC0": "Глубокий желтый",
        "007CAD": "Глубокий желтый зеленый",
        "E0B0FF": "Глубокий зеленовато-желтый",
        "CD00CD": "Глубокий зеленый"
    ]
    
    public init() {
    }
    
    func hexStringToUIColor(hex: String) -> UIColor? {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return nil
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func getHexFromColor(_ color: Colors) -> String {
        color.rawValue
    }
    
    func getRusColorName(fromHex: String) -> String {
        
        guard
            let colorName = colorsDict[fromHex]
        else {
            return "Цвет отсутствует в палитре"
        }
        
        return colorName
    }
}
