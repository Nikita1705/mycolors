import XCTest
@testable import MyColors

final class MyColorsTests: XCTestCase {
    
    private var myColors: MyColors!
    
    override func setUp() {
        super.setUp()
        
        self.myColors = MyColors()
    }
    
    override func tearDown() {
        super.tearDown()
        
        self.myColors = nil
    }
    func testSearchingColorNotNil() {
        let colorHex = Colors.AmaranthLightCherry.rawValue
        
        let color = myColors.hexStringToUIColor(hex: colorHex)
        
        XCTAssertNotNil(color, "Color conversation is failure")
    }
    
    func testGettingColorHexFromEqualGivenColor() {
        let color = Colors.AmaranthLightCherry
        
        let colorHex = self.myColors.getHexFromColor(.AmaranthLightCherry)
        
        XCTAssertEqual(color.rawValue, colorHex, "Hex color don't equal")
    }
    
    func testSearchingRusColorNameNotNil() {
        let colorHex = Colors.AmaranthLightCherry.rawValue
        
        let rusColor = self.myColors.colorsDict[colorHex]
        
        XCTAssertNotNil(rusColor, "Searching color is not in the dictionary")
    }
}
